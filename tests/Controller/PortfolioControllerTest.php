<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class PortfolioControllerTest extends WebTestCase
{
    public function testHomePage(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/nl/');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Dividend tracker');
    }
}
